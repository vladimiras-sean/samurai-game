# Welcome to the Samurai Village Unity Project

## About
This project is designed to provide you with a fluid 3rd person adventure game to test your game audio implementation skills. The Game features small quests to test your knowledge of c#, volumes & triggers, multiple surface types for footsteps, basic character animations and more.

The game is design for use with FMOD however WWISE could be used instead. 

## Unity Version
<b>Last Updated 21/03/2023</b> <br>
<i>Please note that this project has been updated to a newer version of unity. </i>

<b>Current Project Version for Unity LTS 2021.3.19f1 for Apple Silicon (also works with Windows/Linux)</b>

<i>Previous Version 2021.3.9 LTS</i>

### Contributors
- Managed by SOUTH LOOP STUDIOS, BIRMINGHAM
- Assets & Basic Level Design by Synty Studios (Samurai Pack)
- Programming by Ben Weatherill

### How to download?
You can either do 1 of the following:
- Pull this repo to your mac/pc & then place in your own repo. I recommend using a GitLab repo with GitLFS enabled. GitHub is not recommended due to its 2GB repo size. 
- Fork this repo to your GitLab account

### Upcoming Features
- Full Menu System with Sliders designed for Volume (Music, SFX, Dialogue)
- Start Screen Scene
- Saving system (summer 2023)

### Known Issues
- Player can get stuck in stairs

### Resolved Issues
N/A

